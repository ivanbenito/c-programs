#include <stdio.h>
#include <stdlib.h>

/* Funcion punto de entrada */
int main (){
	int edad;

	printf("¿Cuantos años tienes?: "); // stdout
	scanf("%i", &edad);
	printf("Tienes, %i años\n",edad);

	return EXIT_SUCCESS;
}
