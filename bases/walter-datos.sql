INSERT INTO PERSONA VALUES
('Walter','White','Profesor'),
('Jessie','Pinkman','Dealer'),
('Saul','Goodman','Abogado');

INSERT INTO OBJETO VALUES
('Coche','grande',NULL),
('Mochila','mediano','Coche'),
('Pistola','pequeño','Mochila');

INSERT INTO SITUACION VALUES
('9','Caravana','Jessie' ,'Pinkman','Heisenberg','Baby blue'),
('10','Despacho','Saul','Goodman','Traje','Pasta $$$');

INSERT INTO lleva VALUES
('9','Pistola'),
('10','Mochila');
