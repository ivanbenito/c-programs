#include <stdlib.h>
#include <stdio.h>

/* Funcion punto de entrada */
int main(){

  int l = 5;

  for (int f=0; f<l; f++){
      for (int c=0; c<l; c++)
          if ( f==0 || f==l-1 || c==0 || c==l-1 || c==f || c+f==l-1)
          printf ("*");
          else
          printf(" ");
               
      printf ("\n");
  }

    return EXIT_SUCCESS;
}
