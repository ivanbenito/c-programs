#include <stdlib.h>
#include <stdio.h>

/* Funcion punto de entrada */
int main(){

    int numero, resto, i;

    printf("Programa para sacar todos los divisores de un numero\n");
    printf("Dime un numero:\n ");
    scanf("%d", &numero);
    printf("Los divisores de este numero son:\n");
          for (i=1; i<=numero; i++)
          {
              if (numero%i==0){
                 printf("%d\n",i);
              }
          }

    return EXIT_SUCCESS;
}
