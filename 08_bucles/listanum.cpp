#include <stdlib.h>
#include <stdio.h>

#define N 10

/* Funcion punto de entrada */
int main(){

    unsigned lista[N];

    for(int i=0; i<N; i++)
        printf("lista[%i] = %u\n", i, lista[i]);

    printf("\n");

    return EXIT_SUCCESS;
}
