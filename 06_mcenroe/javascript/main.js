var X = 0
var Y = 1
var lienzo
var WIDTH =850 
var HEIGHT =600 
var XC = WIDTH / 2
var YC = HEIGHT / 2
var ndiv = 1000
var tabla = 1020 
var marca = [ ]

function p2c(radio, ang){
    return [ radio * Math.cos(rad(ang)), radio * Math.sin(rad(ang))]
}

function rad(angulo) {
    return angulo * Math.PI / 180
}

function colorl(valor) {
    lienzo.strokeStyle = valor;
}

function cc(coord){
    return [XC + coord[0], YC - coord[1]]
}

function linea(p1, p2){
    var c1 = cc(p1)
    var c2 = cc(p2)
    lienzo.moveTo(c1[X], c1[Y])
    lienzo.lineTo(c2[X], c2[Y])
}

function main() {

    lienzo = document.getElementById("lienzo").getContext("2d")
    var radio = 200
    var extra = 5

  for (var i=0; i<ndiv; i++)
    marca[i] = p2c(radio, i *360 / ndiv)
  
  /* Calculo donde estan las marcas */
 // for (var angulo = 00; angulo<360; angulo+=360/10)
 //       marca.push( [radio * Math.cos( rad(angulo)),
 //                    radio * Math.sin( rad(angulo))
 //                    ])

    lienzo.beginPath()
    lienzo.arc(cc([0,0])[X], cc([0,0])[Y], radio, 0, Math.PI * 2)
    colorl = ("#DD33CC")
    lienzo.stroke()

    lienzo.beginPath()
    colorl = ("#CC3333")
    for (var angulo = 00; angulo<360; angulo+=360/ndiv)
        linea( p2c(radio-extra, angulo), p2c(radio+extra, angulo) )
    lienzo.stroke()

    lienzo.beginPath()
    for (var i=1; i<ndiv; i++)
        linea(marca[i], marca[tabla*i % ndiv])
    colorl = ("#000066")
    lienzo.stroke()

}
