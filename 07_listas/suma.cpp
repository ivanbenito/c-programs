#include <stdio.h>
#include <stdlib.h>

#define N 5

int main(){

  int emento[N];

  /* salida de entornos */
  emento[1] = emento[0] = 1;
 
  /* calculos */
  for (int i = 2; i<N; i++)
       emento[i] = emento[i-1] + emento[i-2];

  /* salida de datos */
  for (int i=0; i<N; i++)
      printf ("%i ", emento[i]);
  printf ("\n");

  for (int i=1; 1<N; i++)
      printf ("%.4lf ", (double) emento[i] / emento[i-1]);
  printf ("\n");
  
  return EXIT_SUCCESS;
}
