#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100

void pon_titulo(int tabla){ /* Parámetro formal */
	char titulo[MAX];
	sprinf(titulo, "toilet -fpagga --metal Tabla del %i", tabla); 
	system(titulo);
}

/* Funcion punto de entrada */
int main(){
	/* declaracion de variable */
	int tabla, op1 = 1;

	/* menu */
	printf("¿Qué tabla quieres?\n");
	printf("Tabla: ");
	scanf(" %i", &tabla);
	
	pon_titulo(tabla); /* Llamada con parámetro actual 5 */
	/* puedes poner un numero o la variable */
	/* resultados */
	printf("%1x%i=%1\n",op1, tabla, op1 * tabla);
	op1++;
	printf("%ix%1=%i\n",op1, tabla, op1 * tabla);

	return EXIT_SUCCESS;
}
