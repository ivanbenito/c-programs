#include <stdio.h>
#include <stdlib.h>

/* Funcion punto de entrada */
int main() {
    int resultado;  
    int a;
    int b;
    int opcion;

    printf("Esto te permitirá saber el área del cuadrado(1), rectángulo(2), rombo(3) o triangulo(4).\n");
    printf("Indique el número de la forma geometrica que quieres calcular: ");
    scanf("%i", &opcion);

    switch(opcion)
    {
        case 1:
            printf("\n Pon el valor del lado: ");
            scanf("%i", &a);
            resultado=a*a;
            printf("\nEl área del cuadrado es: %i\n", resultado);
            break;

        case 2:
            printf("\n Pon el valor de la altura: ");
            scanf("%i", &a);
            printf("\n Pon el valor de la base: ");
            scanf("%i", &b);
            resultado=a*b;
            printf("\nEl área del rectángulo es: %i\n", resultado);
            break;

        case 3:
            printf("\n Pon el valor de la diagonal menor: ");
            scanf("%i", &a);
            printf("\n Pon el valor de la diagonal mayor: ");
            scanf("%i", &b);
            resultado=(a*b)/2;
            printf("\nEl área del rombo es: %i\n", resultado);
            break;

        case 4:
            printf("\n Pon el valor de la altura: ");
            scanf("%i", &a);
            printf("\n Pon el valor de la base: ");
            scanf("%i", &b);
            resultado=(a*b)/2;
            printf("\n El area del triangulo es: %i\n", resultado);
            break;

    }

    return EXIT_SUCCESS;


}
